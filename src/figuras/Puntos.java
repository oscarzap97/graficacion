package figuras;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.LinkedList;

public class Puntos {

	private LinkedList<Point2D> puntos = new LinkedList<Point2D>();
	private Color color = Color.BLACK;
	
	public void addPunto(Point2D punto)
    {
        puntos.add(punto);
    }
	
	public void addPunto(float x, float y)
    {
        Point2D p = new Point2D.Float(x, y);
        puntos.add(p);
    }
	
	public LinkedList<Point2D> getPuntos()
    {
        return puntos;
    }
	
	public Point2D getPunto(int posicion)
    {
        return puntos.get(posicion);
    }
	
	public Color getColor()
	{
		return this.color;
	}
	
	public int getSize()
	{
		return this.puntos.size();
	}
	
}
